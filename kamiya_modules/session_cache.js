let cache = {};
let check = {};

function getSession(p) {
    return cache[p];
}

function setSession(p,t) {
    cache[p] = t;
    return true;
}

function getCheck(p) {
    return check[p];
}

function setCheck(p) {
    check[p] = true;
    return true;
}

module.exports = {
    getSession: getSession,
    setSession: setSession,
    getCheck: getCheck,
    setCheck: setCheck
};